using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MoveTheBoxLevelDesigner
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            richTextBox_controls.Text = 
                "Move around: cursors (up/down/left/right)\r\n" + 
                "Place wall field:        W\r\n" + 
                "Place goal field:        G\r\n" + 
                "Place a box:             B\r\n" + 
                "Place man:               S\r\n" +
                "Place box on goal field: V\r\n" +
                "Place man on goal field: A\r\n" +
                "Delete a field:          D";
        }

        private void button_backgroundOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}