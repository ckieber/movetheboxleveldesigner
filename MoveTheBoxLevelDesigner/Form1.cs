using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PositionLibrary;
using LevelLibrary;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace MoveTheBoxLevelDesigner
{
    public partial class Form1 : Form
    {
        public Panel[,] _panelMatrix;
        public Color _wallColor;
        public Color _boxColor;
        public Color _goalColor;
        public Color _groundColor;
        public Color _startPosColor;
        public Color _boxOnGoalColor;
        public Color _manOnGoalColor;
        private Bitmap _markField;
        private bool _released;
        public bool _startPosSet;
        private Position _currentPos;
        public Levels _levelContainer;
        public string _filePath;

        public Form1()
        {
            InitializeComponent();
            _wallColor = Color.SaddleBrown;
            _boxColor = Color.Peru;
            _goalColor = Color.YellowGreen;
            _groundColor = Color.Wheat;
            _startPosColor = Color.Blue;
            _boxOnGoalColor = Color.Orange;
            _manOnGoalColor = Color.Aqua;
            _released = true;
            _startPosSet = false;
            _currentPos = new Position(0, 0);
            _markField = (Bitmap)Bitmap.FromFile("Textures/markField.bmp");
            _markField.MakeTransparent(Color.White);
            _levelContainer = new Levels();
            _filePath = "";

            //initialize panel matrix
            _panelMatrix = new Panel[25, 25] {{panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8, panel9, panel10, panel11, panel12, panel13, panel14, panel15, panel16, panel17, panel18, panel19, panel20, panel21, panel22, panel23, panel24, panel25},
                                              {panel41, panel42, panel43, panel44, panel45, panel46, panel47, panel48, panel49, panel50, panel51, panel52, panel53, panel54, panel55, panel56, panel57, panel58, panel59, panel60, panel61, panel62, panel63, panel64, panel65},
                                              {panel81, panel82, panel83, panel84, panel85, panel86, panel87, panel88, panel89, panel90, panel91, panel92, panel93, panel94, panel95, panel96, panel97, panel98, panel99, panel100, panel101, panel102, panel103, panel104, panel105},
                                              {panel121, panel122, panel123, panel124, panel125, panel126, panel127, panel128, panel129, panel130, panel131, panel132, panel133, panel134, panel135, panel136, panel137, panel138, panel139, panel140, panel141, panel142, panel143, panel144, panel145},
                                              {panel161, panel162, panel163, panel164, panel165, panel166, panel167, panel168, panel169, panel170, panel171, panel172, panel173, panel174, panel175, panel176, panel177, panel178, panel179, panel180, panel181, panel182, panel183, panel184, panel185},
                                              {panel201, panel202, panel203, panel204, panel205, panel206, panel207, panel208, panel209, panel210, panel211, panel212, panel213, panel214, panel215, panel216, panel217, panel218, panel219, panel220, panel221, panel222, panel223, panel224, panel225},
                                              {panel241, panel242, panel243, panel244, panel245, panel246, panel247, panel248, panel249, panel250, panel251, panel252, panel253, panel254, panel255, panel256, panel257, panel258, panel259, panel260, panel261, panel262, panel263, panel264, panel265},
                                              {panel281, panel282, panel283, panel284, panel285, panel286, panel287, panel288, panel289, panel290, panel291, panel292, panel293, panel294, panel295, panel296, panel297, panel298, panel299, panel300, panel301, panel302, panel303, panel304, panel305},
                                              {panel321, panel322, panel323, panel324, panel325, panel326, panel327, panel328, panel329, panel330, panel331, panel332, panel333, panel334, panel335, panel336, panel337, panel338, panel339, panel340, panel341, panel342, panel343, panel344, panel345},
                                              {panel361, panel362, panel363, panel364, panel365, panel366, panel367, panel368, panel369, panel370, panel371, panel372, panel373, panel374, panel375, panel376, panel377, panel378, panel379, panel380, panel381, panel382, panel383, panel384, panel385},
                                              {panel401, panel402, panel403, panel404, panel405, panel406, panel407, panel408, panel409, panel410, panel411, panel412, panel413, panel414, panel415, panel416, panel417, panel418, panel419, panel420, panel421, panel422, panel423, panel424, panel425},
                                              {panel441, panel442, panel443, panel444, panel445, panel446, panel447, panel448, panel449, panel450, panel451, panel452, panel453, panel454, panel455, panel456, panel457, panel458, panel459, panel460, panel461, panel462, panel463, panel464, panel465},
                                              {panel481, panel482, panel483, panel484, panel485, panel486, panel487, panel488, panel489, panel490, panel491, panel492, panel493, panel494, panel495, panel496, panel497, panel498, panel499, panel500, panel501, panel502, panel503, panel504, panel505},
                                              {panel521, panel522, panel523, panel524, panel525, panel526, panel527, panel528, panel529, panel530, panel531, panel532, panel533, panel534, panel535, panel536, panel537, panel538, panel539, panel540, panel541, panel542, panel543, panel544, panel545},
                                              {panel561, panel562, panel563, panel564, panel565, panel566, panel567, panel568, panel569, panel570, panel571, panel572, panel573, panel574, panel575, panel576, panel577, panel578, panel579, panel580, panel581, panel582, panel583, panel584, panel585},
                                              {panel601, panel602, panel603, panel604, panel605, panel606, panel607, panel608, panel609, panel610, panel611, panel612, panel613, panel614, panel615, panel616, panel617, panel618, panel619, panel620, panel621, panel622, panel623, panel624, panel625},
                                              {panel641, panel642, panel643, panel644, panel645, panel646, panel647, panel648, panel649, panel650, panel651, panel652, panel653, panel654, panel655, panel656, panel657, panel658, panel659, panel660, panel661, panel662, panel663, panel664, panel665},
                                              {panel681, panel682, panel683, panel684, panel685, panel686, panel687, panel688, panel689, panel690, panel691, panel692, panel693, panel694, panel695, panel696, panel697, panel698, panel699, panel700, panel701, panel702, panel703, panel704, panel705},
                                              {panel721, panel722, panel723, panel724, panel725, panel726, panel727, panel728, panel729, panel730, panel731, panel732, panel733, panel734, panel735, panel736, panel737, panel738, panel739, panel740, panel741, panel742, panel743, panel744, panel745},
                                              {panel761, panel762, panel763, panel764, panel765, panel766, panel767, panel768, panel769, panel770, panel771, panel772, panel773, panel774, panel775, panel776, panel777, panel778, panel779, panel780, panel781, panel782, panel783, panel784, panel785},
                                              {panel801, panel802, panel803, panel804, panel805, panel806, panel807, panel808, panel809, panel810, panel811, panel812, panel813, panel814, panel815, panel816, panel817, panel818, panel819, panel820, panel821, panel822, panel823, panel824, panel825},
                                              {panel841, panel842, panel843, panel844, panel845, panel846, panel847, panel848, panel849, panel850, panel851, panel852, panel853, panel854, panel855, panel856, panel857, panel858, panel859, panel860, panel861, panel862, panel863, panel864, panel865},
                                              {panel881, panel882, panel883, panel884, panel885, panel886, panel887, panel888, panel889, panel890, panel891, panel892, panel893, panel894, panel895, panel896, panel897, panel898, panel899, panel900, panel901, panel902, panel903, panel904, panel905},
                                              {panel921, panel922, panel923, panel924, panel925, panel926, panel927, panel928, panel929, panel930, panel931, panel932, panel933, panel934, panel935, panel936, panel937, panel938, panel939, panel940, panel941, panel942, panel943, panel944, panel945},
                                              {panel961, panel962, panel963, panel964, panel965, panel966, panel967, panel968, panel969, panel970, panel971, panel972, panel973, panel974, panel975, panel976, panel977, panel978, panel979, panel980, panel981, panel982, panel983, panel984, panel985}};

            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    _panelMatrix[row, col].BackColor = _groundColor;
                }
            }
            _panelMatrix[0, 0].BackgroundImage = _markField;
        }

        private void loadLevels()
        {
            try
            {
                Stream stream = File.Open(_filePath, FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                _levelContainer = (Levels)formatter.Deserialize(stream);
                stream.Close();
            }
            catch { }          
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (_released)
            {
                int row = _currentPos.getRow();
                int col = _currentPos.getCol();

                if (e.KeyCode == Keys.Left)
                {
                    _panelMatrix[row, col].BackgroundImage = null;
                    if (col - 1 >= 0)
                    {
                        col--;
                    }
                    _panelMatrix[row, col].BackgroundImage = _markField;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    _panelMatrix[row, col].BackgroundImage = null;
                    if (col + 1 < 25)
                    {
                        col++;
                    }
                    _panelMatrix[row, col].BackgroundImage = _markField;
                }
                else if (e.KeyCode == Keys.Up)
                {
                    _panelMatrix[row, col].BackgroundImage = null;
                    if (row - 1 >= 0)
                    {
                        row--;
                    }
                    _panelMatrix[row, col].BackgroundImage = _markField;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    _panelMatrix[row, col].BackgroundImage = null;
                    if (row + 1 < 25)
                    {
                        row++;
                    }
                    _panelMatrix[row, col].BackgroundImage = _markField;
                }
                else if (e.KeyCode == Keys.W)
                {
                    if (_panelMatrix[row, col].BackColor == _startPosColor)
                    {
                        _startPosSet = false;
                    }
                    _panelMatrix[row, col].BackColor = _wallColor;
                }
                else if (e.KeyCode == Keys.G)
                {
                    if (_panelMatrix[row, col].BackColor == _startPosColor)
                    {
                        _startPosSet = false;
                    }
                    _panelMatrix[row, col].BackColor = _goalColor;
                }
                else if (e.KeyCode == Keys.B)
                {
                    if (_panelMatrix[row, col].BackColor == _startPosColor)
                    {
                        _startPosSet = false;
                    }
                    _panelMatrix[row, col].BackColor = _boxColor;
                }
                else if (e.KeyCode == Keys.V)
                {
                    if (_panelMatrix[row, col].BackColor == _startPosColor)
                    {
                        _startPosSet = false;
                    }
                    _panelMatrix[row, col].BackColor = _boxOnGoalColor;
                }
                else if (e.KeyCode == Keys.S)
                {
                    if (!_startPosSet)
                    {
                        _panelMatrix[row, col].BackColor = _startPosColor;
                        _startPosSet = true;
                    }
                }
                else if (e.KeyCode == Keys.A)
                {
                    if (!_startPosSet)
                    {
                        _panelMatrix[row, col].BackColor = _manOnGoalColor;
                        _startPosSet = true;
                    }
                }
                else if (e.KeyCode == Keys.D)
                {
                    if (_panelMatrix[row, col].BackColor == _startPosColor ||
                        _panelMatrix[row, col].BackColor == _manOnGoalColor)
                    {
                        _startPosSet = false;
                    }
                    _panelMatrix[row, col].BackColor = _groundColor;
                }

                _currentPos.setRow(row);
                _currentPos.setCol(col);

                _released = false;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            _released = true;
        }

        private void manageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 save = new Form2();
            save.form1 = this;
            save.ShowDialog();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    _panelMatrix[row, col].BackColor = _groundColor;
                    _panelMatrix[row, col].BackgroundImage = null;
                }
            }
            _panelMatrix[0, 0].BackgroundImage = _markField;
            _currentPos = new Position(0,0);
            _startPosSet = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void controlsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 controls = new Form4();
            controls.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form5 about = new Form5();
            about.ShowDialog();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Please select the path where the \"levels.lvl\" file is located.\n";
            folderBrowserDialog1.Description += "Default location is the path of the .exe file.";
            folderBrowserDialog1.ShowDialog();
            _filePath = folderBrowserDialog1.SelectedPath;

            if (_filePath == "")
            {
                _filePath = "levels.lvl";
            }
            else
            {
                _filePath += "\\levels.lvl";
            }
            loadLevels();
        }
    }
}