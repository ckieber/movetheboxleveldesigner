namespace MoveTheBoxLevelDesigner
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.button_backgroundOk = new System.Windows.Forms.Button();
            this.richTextBox_controls = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button_backgroundOk
            // 
            this.button_backgroundOk.BackColor = System.Drawing.Color.Wheat;
            this.button_backgroundOk.Location = new System.Drawing.Point(274, 210);
            this.button_backgroundOk.Name = "button_backgroundOk";
            this.button_backgroundOk.Size = new System.Drawing.Size(83, 31);
            this.button_backgroundOk.TabIndex = 2;
            this.button_backgroundOk.Text = "OK";
            this.button_backgroundOk.UseVisualStyleBackColor = false;
            this.button_backgroundOk.Click += new System.EventHandler(this.button_backgroundOk_Click);
            // 
            // richTextBox_controls
            // 
            this.richTextBox_controls.BackColor = System.Drawing.Color.Wheat;
            this.richTextBox_controls.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_controls.Location = new System.Drawing.Point(12, 12);
            this.richTextBox_controls.Name = "richTextBox_controls";
            this.richTextBox_controls.ReadOnly = true;
            this.richTextBox_controls.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox_controls.Size = new System.Drawing.Size(345, 192);
            this.richTextBox_controls.TabIndex = 3;
            this.richTextBox_controls.Text = "";
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(363, 244);
            this.Controls.Add(this.richTextBox_controls);
            this.Controls.Add(this.button_backgroundOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(379, 280);
            this.MinimumSize = new System.Drawing.Size(379, 280);
            this.Name = "Form4";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Controls";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_backgroundOk;
        private System.Windows.Forms.RichTextBox richTextBox_controls;
    }
}