namespace MoveTheBoxLevelDesigner
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox_levels = new System.Windows.Forms.RichTextBox();
            this.button_save = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.textBox_levelNumber = new System.Windows.Forms.TextBox();
            this.button_preview = new System.Windows.Forms.Button();
            this.button_delete = new System.Windows.Forms.Button();
            this.button_change = new System.Windows.Forms.Button();
            this.textBox_changeTo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_switch = new System.Windows.Forms.Button();
            this.button_edit = new System.Windows.Forms.Button();
            this.button_sort = new System.Windows.Forms.Button();
            this.button_insert = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(78, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Level #: ";
            // 
            // richTextBox_levels
            // 
            this.richTextBox_levels.BackColor = System.Drawing.Color.Wheat;
            this.richTextBox_levels.Location = new System.Drawing.Point(8, 8);
            this.richTextBox_levels.Name = "richTextBox_levels";
            this.richTextBox_levels.ReadOnly = true;
            this.richTextBox_levels.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox_levels.Size = new System.Drawing.Size(64, 210);
            this.richTextBox_levels.TabIndex = 1;
            this.richTextBox_levels.Text = "";
            this.richTextBox_levels.TextChanged += new System.EventHandler(this.richTextBox_levels_TextChanged);
            // 
            // button_save
            // 
            this.button_save.BackColor = System.Drawing.Color.Wheat;
            this.button_save.Location = new System.Drawing.Point(113, 110);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(72, 32);
            this.button_save.TabIndex = 3;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = false;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // button_ok
            // 
            this.button_ok.BackColor = System.Drawing.Color.Wheat;
            this.button_ok.Location = new System.Drawing.Point(233, 148);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(72, 32);
            this.button_ok.TabIndex = 4;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = false;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // textBox_levelNumber
            // 
            this.textBox_levelNumber.BackColor = System.Drawing.Color.Wheat;
            this.textBox_levelNumber.Location = new System.Drawing.Point(133, 8);
            this.textBox_levelNumber.Name = "textBox_levelNumber";
            this.textBox_levelNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox_levelNumber.Size = new System.Drawing.Size(52, 20);
            this.textBox_levelNumber.TabIndex = 6;
            this.textBox_levelNumber.TextChanged += new System.EventHandler(this.textBox_levelNumber_TextChanged);
            // 
            // button_preview
            // 
            this.button_preview.BackColor = System.Drawing.Color.Wheat;
            this.button_preview.Location = new System.Drawing.Point(113, 72);
            this.button_preview.Name = "button_preview";
            this.button_preview.Size = new System.Drawing.Size(72, 32);
            this.button_preview.TabIndex = 7;
            this.button_preview.Text = "Preview";
            this.button_preview.UseVisualStyleBackColor = false;
            this.button_preview.Click += new System.EventHandler(this.button_preview_Click);
            // 
            // button_delete
            // 
            this.button_delete.BackColor = System.Drawing.Color.Wheat;
            this.button_delete.Location = new System.Drawing.Point(113, 186);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(72, 32);
            this.button_delete.TabIndex = 8;
            this.button_delete.Text = "Delete";
            this.button_delete.UseVisualStyleBackColor = false;
            this.button_delete.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // button_change
            // 
            this.button_change.BackColor = System.Drawing.Color.Wheat;
            this.button_change.Location = new System.Drawing.Point(233, 34);
            this.button_change.Name = "button_change";
            this.button_change.Size = new System.Drawing.Size(72, 32);
            this.button_change.TabIndex = 9;
            this.button_change.Text = "Change";
            this.button_change.UseVisualStyleBackColor = false;
            this.button_change.Click += new System.EventHandler(this.button_change_Click);
            // 
            // textBox_changeTo
            // 
            this.textBox_changeTo.BackColor = System.Drawing.Color.Wheat;
            this.textBox_changeTo.Location = new System.Drawing.Point(253, 8);
            this.textBox_changeTo.Name = "textBox_changeTo";
            this.textBox_changeTo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox_changeTo.Size = new System.Drawing.Size(52, 20);
            this.textBox_changeTo.TabIndex = 10;
            this.textBox_changeTo.TextChanged += new System.EventHandler(this.textBox_changeTo_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(191, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "to/with #: ";
            // 
            // button_switch
            // 
            this.button_switch.BackColor = System.Drawing.Color.Wheat;
            this.button_switch.Location = new System.Drawing.Point(233, 72);
            this.button_switch.Name = "button_switch";
            this.button_switch.Size = new System.Drawing.Size(72, 32);
            this.button_switch.TabIndex = 12;
            this.button_switch.Text = "Switch";
            this.button_switch.UseVisualStyleBackColor = false;
            this.button_switch.Click += new System.EventHandler(this.button_switch_Click);
            // 
            // button_edit
            // 
            this.button_edit.BackColor = System.Drawing.Color.Wheat;
            this.button_edit.Location = new System.Drawing.Point(113, 34);
            this.button_edit.Name = "button_edit";
            this.button_edit.Size = new System.Drawing.Size(72, 32);
            this.button_edit.TabIndex = 13;
            this.button_edit.Text = "Edit";
            this.button_edit.UseVisualStyleBackColor = false;
            this.button_edit.Click += new System.EventHandler(this.button_edit_Click);
            // 
            // button_sort
            // 
            this.button_sort.BackColor = System.Drawing.Color.Wheat;
            this.button_sort.Location = new System.Drawing.Point(233, 110);
            this.button_sort.Name = "button_sort";
            this.button_sort.Size = new System.Drawing.Size(72, 32);
            this.button_sort.TabIndex = 14;
            this.button_sort.Text = "Sort";
            this.button_sort.UseVisualStyleBackColor = false;
            this.button_sort.Click += new System.EventHandler(this.button_sort_Click);
            // 
            // button_insert
            // 
            this.button_insert.BackColor = System.Drawing.Color.Wheat;
            this.button_insert.Location = new System.Drawing.Point(113, 148);
            this.button_insert.Name = "button_insert";
            this.button_insert.Size = new System.Drawing.Size(72, 32);
            this.button_insert.TabIndex = 15;
            this.button_insert.Text = "Insert";
            this.button_insert.UseVisualStyleBackColor = false;
            this.button_insert.Click += new System.EventHandler(this.button_insert_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(312, 221);
            this.Controls.Add(this.button_insert);
            this.Controls.Add(this.button_sort);
            this.Controls.Add(this.button_edit);
            this.Controls.Add(this.button_switch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_changeTo);
            this.Controls.Add(this.button_change);
            this.Controls.Add(this.button_delete);
            this.Controls.Add(this.button_preview);
            this.Controls.Add(this.textBox_levelNumber);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.richTextBox_levels);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(328, 257);
            this.MinimumSize = new System.Drawing.Size(328, 257);
            this.Name = "Form2";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox_levels;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.TextBox textBox_levelNumber;
        private System.Windows.Forms.Button button_preview;
        private System.Windows.Forms.Button button_delete;
        private System.Windows.Forms.Button button_change;
        private System.Windows.Forms.TextBox textBox_changeTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_switch;
        private System.Windows.Forms.Button button_edit;
        private System.Windows.Forms.Button button_sort;
        private System.Windows.Forms.Button button_insert;
    }
}