using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PositionLibrary;
using LevelLibrary;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace MoveTheBoxLevelDesigner
{
    public partial class Form2 : Form
    {
        public Form1 form1;
        private string _break = "---\r\n";
        public int _previewLevel;
        private List<Position> _wallFields;
        private List<Position> _boxFields;
        private List<Position> _goalFields;
        private Position _startPos; 

        public Form2()
        {
            InitializeComponent();
            _wallFields = new List<Position>();
            _boxFields = new List<Position>();
            _goalFields = new List<Position>();
            _startPos = new Position();

            button_delete.Enabled = false;
            button_preview.Enabled = false;
            button_save.Enabled = false;
            button_change.Enabled = false;
            button_switch.Enabled = false;
            button_edit.Enabled = false;
            button_sort.Enabled = false;
            button_insert.Enabled = false;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            loadTextBox();
        }

        private void button_preview_Click(object sender, EventArgs e)
        {
            _previewLevel = int.Parse(textBox_levelNumber.Text);
            if (form1._levelContainer.getLevel(_previewLevel) != null)
            {
                Form3 preview = new Form3();
                preview.form2 = this;
                preview.ShowDialog();
            }
            emptyTextBoxes();
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            List<Position> wallFields = new List<Position>();
            List<Position> goalFields = new List<Position>();
            List<Position> boxFields = new List<Position>();

            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    if (form1._panelMatrix[row, col].BackColor == form1._wallColor)
                    {
                        _wallFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._goalColor)
                    {
                        _goalFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._boxColor)
                    {
                        _boxFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._startPosColor)
                    {
                        _startPos = new Position(row, col);
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._boxOnGoalColor)
                    {
                        _boxFields.Add(new Position(row, col));
                        _goalFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._manOnGoalColor)
                    {
                        _startPos = new Position(row, col);
                        _goalFields.Add(new Position(row, col));
                    }
                }
            }

            if (form1._levelContainer.getLevel(int.Parse(textBox_levelNumber.Text)) != null)
            {
                form1._levelContainer.updateLevel(int.Parse(textBox_levelNumber.Text), _startPos, _wallFields, _boxFields, _goalFields);
            }
            else
            {
                form1._levelContainer.addLevel(int.Parse(textBox_levelNumber.Text), _startPos, _wallFields, _boxFields, _goalFields);
            }
            saveToFile();
            emptyTextBoxes();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            if (form1._levelContainer.deleteLevel(int.Parse(textBox_levelNumber.Text)))
            {
                saveToFile();
            }
            emptyTextBoxes();
        }

        private void loadTextBox()
        {
            if (form1._levelContainer.getCount() == 0)
            {
                richTextBox_levels.Text = "NA";
            }
            else
            {
                List<int> levels = form1._levelContainer.getAllLevelNumbers();
                richTextBox_levels.Text = _break;
                foreach (int number in levels)
                {
                    richTextBox_levels.Text += number.ToString().PadLeft(2,'0') + "\r\n";
                    richTextBox_levels.Text += _break;
                    richTextBox_levels.SelectionStart = richTextBox_levels.Text.Length;
                    richTextBox_levels.ScrollToCaret();
                }
            }
        }

        private void textBox_levelNumber_TextChanged(object sender, EventArgs e)
        {
            if (textBox_levelNumber.Text == "")
            {
                button_delete.Enabled = false;
                button_preview.Enabled = false;
                button_save.Enabled = false;
                button_edit.Enabled = false;
                button_change.Enabled = false;
                button_switch.Enabled = false;
                button_insert.Enabled = false;
            }
            else
            {
                if (form1._levelContainer.getAllLevelNumbers().Contains(int.Parse(textBox_levelNumber.Text))
                    && form1._levelContainer.isContinuous())
                {
                    button_insert.Enabled = true;
                }
                else
                {
                    button_insert.Enabled = false;
                }

                if (textBox_changeTo.Text != "")
                {
                    button_change.Enabled = true;
                    button_switch.Enabled = true;
                }
                button_delete.Enabled = true;
                button_preview.Enabled = true;
                button_save.Enabled = true;
                button_edit.Enabled = true;
            }
        }

        private void button_change_Click(object sender, EventArgs e)
        {
            int oldLevel = int.Parse(textBox_levelNumber.Text);
            int newLevel = int.Parse(textBox_changeTo.Text);

            if (form1._levelContainer.getLevel(newLevel) == null)
            {
                if (form1._levelContainer.changeLevelNumber(oldLevel, newLevel))
                {
                    saveToFile();
                }
                emptyTextBoxes();
            }
            else
            {
                Form6 error = new Form6();
                error.ShowDialog();
            }
        }

        private void saveToFile()
        {
            try
            {
                Stream stream = File.Create(form1._filePath);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, form1._levelContainer);
                stream.Close();

                loadTextBox();
            }
            catch { }
        }

        private void textBox_changeTo_TextChanged(object sender, EventArgs e)
        {
            if (textBox_changeTo.Text == "" || textBox_levelNumber.Text == "")
            {
                button_change.Enabled = false;
                button_switch.Enabled = false;
            }
            else
            {
                button_change.Enabled = true;
                button_switch.Enabled = true;
            }
        }

        private void button_switch_Click(object sender, EventArgs e)
        {
            int firstLevel = int.Parse(textBox_levelNumber.Text);
            int secondLevel = int.Parse(textBox_changeTo.Text);

            if (form1._levelContainer.switchLevelNumbers(firstLevel, secondLevel))
            {
                saveToFile();
            }
            emptyTextBoxes();
        }

        private void button_edit_Click(object sender, EventArgs e)
        {
            _previewLevel = int.Parse(textBox_levelNumber.Text);
            Level level = form1._levelContainer.getLevel(_previewLevel);
            if (level != null)
            {
                for (int row = 0; row < 25; row++)
                {
                    for (int col = 0; col < 25; col++)
                    {
                        if (level.getBoxFields().Contains(new Position(row, col)) &&
                            level.getGoalFields().Contains(new Position(row, col)))
                        {
                            form1._panelMatrix[row, col].BackColor = form1._boxOnGoalColor;
                        }
                        else if (level.getGoalFields().Contains(new Position(row, col)) &&
                            level.getStartPosition().Equals(new Position(row, col)))
                        {
                            form1._panelMatrix[row, col].BackColor = form1._manOnGoalColor;
                            form1._startPosSet = true;
                        }
                        else if(level.getWallFields().Contains(new Position(row,col)))
                        {
                            form1._panelMatrix[row, col].BackColor = form1._wallColor;
                        }
                        else if (level.getBoxFields().Contains(new Position(row, col)))
                        {
                            form1._panelMatrix[row, col].BackColor = form1._boxColor;
                        }
                        else if (level.getGoalFields().Contains(new Position(row, col)))
                        {
                            form1._panelMatrix[row, col].BackColor = form1._goalColor;
                        }
                        else if (level.getStartPosition().Equals(new Position(row, col)))
                        {
                            form1._panelMatrix[row, col].BackColor = form1._startPosColor;
                            form1._startPosSet = true;
                        }
                        else
                        {
                            form1._panelMatrix[row, col].BackColor = form1._groundColor;
                        }                       
                    }
                }
                this.Close();
            }
        }

        private void button_sort_Click(object sender, EventArgs e)
        {
            form1._levelContainer.sort();
            saveToFile();
            emptyTextBoxes();
        }

        private void richTextBox_levels_TextChanged(object sender, EventArgs e)
        {
            if (richTextBox_levels.Text == "NA")
            {
                button_sort.Enabled = false;
            }
            else
            {
                button_sort.Enabled = true;
            }
        }

        private void button_insert_Click(object sender, EventArgs e)
        {
            List<Position> wallFields = new List<Position>();
            List<Position> goalFields = new List<Position>();
            List<Position> boxFields = new List<Position>();

            for (int row = 0; row < 25; row++)
            {
                for (int col = 0; col < 25; col++)
                {
                    if (form1._panelMatrix[row, col].BackColor == form1._wallColor)
                    {
                        _wallFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._goalColor)
                    {
                        _goalFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._boxColor)
                    {
                        _boxFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._startPosColor)
                    {
                        _startPos = new Position(row, col);
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._boxOnGoalColor)
                    {
                        _boxFields.Add(new Position(row, col));
                        _goalFields.Add(new Position(row, col));
                    }
                    else if (form1._panelMatrix[row, col].BackColor == form1._manOnGoalColor)
                    {
                        _startPos = new Position(row, col);
                        _goalFields.Add(new Position(row, col));
                    }
                }
            }

            form1._levelContainer.insert(int.Parse(textBox_levelNumber.Text), _startPos, _wallFields, _boxFields, _goalFields);
            saveToFile();
            emptyTextBoxes();
        }

        private void emptyTextBoxes()
        {
            textBox_levelNumber.Text = "";
            textBox_changeTo.Text = "";
        }
    }
}